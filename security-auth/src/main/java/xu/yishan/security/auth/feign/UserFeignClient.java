package xu.yishan.security.auth.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xu.yishan.security.common.result.R;

/**
 * @Description: User模块远程接口
 * @author: YF
 * @date: 2021/6/9
 */
@FeignClient("security-user")
public interface UserFeignClient {
    @GetMapping("/user/loadByUsername")
    R loadUserByUsername(@RequestParam("username") String username);
}
