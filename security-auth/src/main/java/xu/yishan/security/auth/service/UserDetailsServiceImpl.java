package xu.yishan.security.auth.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import xu.yishan.security.auth.feign.UserFeignClient;
import xu.yishan.security.auth.to.UserJwt;
import xu.yishan.security.common.model.domain.admin.SysUser;
import xu.yishan.security.common.result.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: UserDetailsService实现类
 * @author: YF
 * @date: 2021/6/9
 */
@Component
@Qualifier("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserFeignClient userFeignClient;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        R r = userFeignClient.loadUserByUsername(s);
        if (!r.getSuccess() || r.getData() == null) {
            throw new UsernameNotFoundException("用户不存在");
        } else {
            SysUser user = Convert.convert(SysUser.class, r.getData());
            List<String> permList = user.getPermList();
            String userPermission = CollUtil.join(permList, ",");
            UserJwt userJwt = new UserJwt(user.getUsername(), user.getPassword(), AuthorityUtils.commaSeparatedStringToAuthorityList(userPermission));
            userJwt.setUserId(user.getUserId());
            return userJwt;
        }
    }
}
