package xu.yishan.security.auth.controller;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;
import xu.yishan.security.common.model.vo.AuthToken;
import xu.yishan.security.common.result.R;
import xu.yishan.security.common.result.ResultCode;

import java.security.Principal;
import java.util.Map;


@RestController
@RequestMapping("oauth")
public class AuthController {
    @Autowired
    private TokenEndpoint tokenEndpoint;
    @ApiOperation("Oauth2获取token")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "grant_type", value = "授权模式", required = true),
            @ApiImplicitParam(name = "client_id", value = "Oauth2客户端ID", required = true),
            @ApiImplicitParam(name = "client_secret", value = "Oauth2客户端秘钥", required = true),
            @ApiImplicitParam(name = "refresh_token", value = "刷新token"),
            @ApiImplicitParam(name = "username", value = "登录用户名"),
            @ApiImplicitParam(name = "password", value = "登录密码")
    })
    @PostMapping("/token")
    public R postAccessToken(@ApiIgnore Principal principal,@ApiIgnore @RequestParam Map<String, String> parameters) throws HttpRequestMethodNotSupportedException {
        OAuth2AccessToken token = tokenEndpoint.postAccessToken(principal, parameters).getBody();
        AuthToken authToken = new AuthToken();
        if (StrUtil.isBlank(token.getValue())){
            return R.failed(ResultCode.FETCH_ACCESS_TOKEN_FAILED);
        }
        authToken.setAccess_token(token.getValue());
        authToken.setRefresh_token(token.getRefreshToken().getValue());
        authToken.setJwi((String) token.getAdditionalInformation().get("jti"));
        return R.ok(authToken);
    }
}
