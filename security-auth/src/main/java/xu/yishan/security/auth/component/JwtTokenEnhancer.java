package xu.yishan.security.auth.component;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;
import xu.yishan.security.auth.to.UserJwt;

import java.util.HashMap;
import java.util.Map;

/*
 * @Author YF
 * @Description JWT内容增强器
 * @Date 10:06 2021/6/25
 **/
@Component
public class JwtTokenEnhancer implements TokenEnhancer {
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
        UserJwt principal = (UserJwt) oAuth2Authentication.getPrincipal();
        Map<String, Object> map = new HashMap<>();
        //把用户ID设置到jwt中
        map.put("userId",principal.getUserId());
        ((DefaultOAuth2AccessToken) oAuth2AccessToken).setAdditionalInformation(map);
        return oAuth2AccessToken;
    }
}
