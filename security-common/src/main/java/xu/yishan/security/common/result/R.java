package xu.yishan.security.common.result;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author YF
 * @Description 全局统一返回结果类
 * @date 2021/5/25
 */
@Data
@ApiModel("全局统一返回结果类")
public class R<T> {
    @ApiModelProperty(value = "返回码")
    private Integer code;

    @ApiModelProperty(value = "返回消息")
    private String message;

    @ApiModelProperty(value = "返回数据")
    private T data;

    @ApiModelProperty(value = "是否成功")
    private Boolean success;


    /**
     * 成功返回结果
     */
    public static <T> R<T> ok() {
        R<T> r = new R<>();
        r.setCode(ResultCode.SUCCESS.getCode());
        r.setSuccess(true);
        r.setMessage(ResultCode.SUCCESS.getMessage());
        return r;
    }

    /**
     * 成功返回结果
     */
    public static <T> R<T> ok(T data) {
        R<T> r = new R<>();
        r.setData(data);
        r.setCode(ResultCode.SUCCESS.getCode());
        r.setSuccess(true);
        r.setMessage(ResultCode.SUCCESS.getMessage());
        return r;
    }

    /**
     * 失败返回结果
     */
    public static <T> R<T> failed(T data) {
        R<T> r = new R<>();
        r.setData(data);
        r.setCode(ResultCode.FAIL.getCode());
        r.setSuccess(false);
        r.setMessage(ResultCode.FAIL.getMessage());
        return r;
    }

    /**
     * 失败返回结果
     *
     * @param resultCode 错误码
     */
    public static <T> R<T> failed(ResultCode resultCode) {
        R<T> r = new R<T>();
        r.setSuccess(false);
        r.setCode(resultCode.getCode());
        r.setMessage(resultCode.getMessage());
        return r;
    }

    public static <T> R<T> failed() {
        R<T> r = new R<T>();
        r.setSuccess(false);
        r.setCode(ResultCode.FAIL.getCode());
        r.setMessage(ResultCode.FAIL.getMessage());
        return r;
    }

    public static <T> R<T> failed(String msg) {
        R<T> r = new R<T>();
        r.setSuccess(false);
        r.setCode(ResultCode.FAIL.getCode());
        r.setMessage(msg);
        return r;
    }

    public static <T> R<T> failed(Integer code, String message) {
        R<T> r = new R<T>();
        r.setSuccess(false);
        r.setCode(code);
        r.setMessage(message);
        return r;
    }
}
