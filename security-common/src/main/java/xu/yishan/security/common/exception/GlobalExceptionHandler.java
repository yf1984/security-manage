package xu.yishan.security.common.exception;


import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import xu.yishan.security.common.result.R;

/**
 * @Description: 全局异常处理
 * @author: YF
 * @date: 2021/5/25
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    @ResponseBody
    @ExceptionHandler(CustomException.class)
    public R error(CustomException e) {
        if (e.getCode() != null) {
            return R.failed(e.getCode(), e.getMessage());
        }
        return R.failed(e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public R error(Exception e) {
        return R.failed(e.getMessage());
    }
}
