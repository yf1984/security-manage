package xu.yishan.security.common.exception;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import xu.yishan.security.common.result.ResultCode;

/**
 * @Description: 自定义全局异常类
 * @author: YF
 * @date: 2021/5/25
 */
@Getter
@ApiModel("自定义全局异常类")
public class CustomException extends RuntimeException {
    @ApiModelProperty(value = "异常状态码")
    private Integer code;

    /**
     * 通过状态码和错误消息创建异常对象
     *
     * @param message 错误消息
     * @param code    状态码
     */
    public CustomException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    public CustomException(String message) {
        super(message);
    }

    public CustomException(Throwable cause) {
        super(cause);
    }

    /**
     * 接收枚举类型对象
     *
     * @param resultCode 状态码
     */
    public CustomException(ResultCode resultCode) {
        super(resultCode.getMessage());
        this.code = resultCode.getCode();
    }

    @Override
    public String toString() {
        return "CustomException{" +
                "code=" + code +
                ", message=" + this.getMessage() +
                '}';
    }
}
