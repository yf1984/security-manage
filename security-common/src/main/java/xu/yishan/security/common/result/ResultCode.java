package xu.yishan.security.common.result;

import lombok.Getter;

/**
 * 统一返回结果状态信息类
 * Created by yf on 2021/3/24
 */
@Getter
public enum ResultCode {
    UNKNOWN_EXCEPTION(100, "未知错误"),
    SUCCESS(200, "成功"),
    FAIL(201, "失败"),
    PARAM_ERROR(202, "参数不正确"),
    ADMIN_NOT_DELETE(203, "系统管理员不允许删除"),
    LOGIN_USER_NOT_DELETE(204, "当前登录用户不能删除"),
    USERNAME_OR_PASSWORD_NULL(205, "用户名或密码不能为空"),
    LOGIN_FAILED(206, "登录失败"),
    ROOT_DEPARTMENT_IS_EXIST(207, "根机构只能有一个"),
    LOGIN_AUTH(208, "未登陆"),
    PERMISSION(209, "没有权限"),

    CODE_ERROR(210, "验证码错误"),
    BASE_ROLE_NOT_DELETE(211, "系统角色，不能删除"),
    LOGIN_DISABLED_ERROR(212, "改用户已被禁用"),
    NEW_PASSWORD_NULL(213, "新密码为空"),
    LOGIN_AURH(214, "需要登录"),
    LOGIN_ACL(215, "没有权限"),

    NEW_OLD_PASSWORD_SAME(216, "新密码和原密码相同"),
    ILLEGAL_CALLBACK_REQUEST_ERROR(217, "非法回调请求"),
    FETCH_ACCESS_TOKEN_FAILED(218, "获取accessToken失败"),
    FETCH_USERINFO_ERROR(219, "获取用户信息失败"),
    BASE_ROLE_NOT_UPDATE(23005, "系统角色，不能修改"),

    PASSWORD_ERROR(220, "密码错误"),
    CONFIRM_PASSWORD_DIFFERENT(221, "两次密码不一致"),
    CAR_NUMBER_EXIST(225, "车牌号已存在"),
    CAR_NUMBER_NULL(226, "车牌号不能为空"),
    CANT_ACROSS_STORES(227, "不能跨店");

    private Integer code;
    private String message;

    private ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}