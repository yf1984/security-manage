package xu.yishan.security.common.model.vo;

import lombok.Data;

/**
 * Created by yf on 2021/4/5
 */
@Data
public class PasswordForm {
    /**
     * 原密码
     */
    private String password;
    /**
     * 新密码
     */
    private String newPassword;
}
