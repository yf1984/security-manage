package xu.yishan.security.common.constant;

/**
 * @Description: admin模块常量
 * @author: YF
 * @date: 2021/5/25
 */
public class AdminConstant {
    public static final Long SUPER_ADMIN = 666L;
    public static final Long SUPER_ROLE = 888L;
    public static final String INIT_PASSWORD = "000000";

    public enum MenuType {
        /**
         * 目录
         */
        CATALOG(0),
        /**
         * 菜单
         */
        MENU(1),
        /**
         * 按钮
         */
        BUTTON(2);

        private int value;

        MenuType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
