package xu.yishan.security.common.model.vo;


import lombok.Data;

/**
 * Created by mrt on 2018/5/21.
 */
@Data
public class AuthToken {
	String access_token;//访问token就是短令牌，用户身份令牌
	String refresh_token;//刷新token
	String jwi;//jwt令牌
}
