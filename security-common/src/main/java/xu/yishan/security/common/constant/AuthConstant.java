package xu.yishan.security.common.constant;

/**
 * @Description: 权限相关常量
 * @author: YF
 * @date: 2021/5/25
 */
public interface AuthConstant {

    /**
     * JWT存储权限属性
     */
    String AUTHORITY_CLAIM_NAME = "authorities";

    /**
     * 后台管理client_id
     */
    String ADMIN_CLIENT_ID = "security_user";


    /**
     * 后台管理接口路径匹配
     */
    String ADMIN_URL_PATTERN = "/office-admin/**";

    /**
     * Redis缓存权限规则key
     */
    String RESOURCE_ROLES_MAP_VALUE = "auth:resourceRolesMap";
    String RESOURCE_ROLES_MAP_KEY = "::initResourceRolesMap";


    /**
     * 认证信息Http请求头
     */
    String JWT_TOKEN_HEADER = "Authorization";

    /**
     * JWT令牌前缀
     */
    String JWT_TOKEN_PREFIX = "Bearer ";

    /**
     * 用户信息Http请求头
     */
    String TOKEN_HEADER = "Token";
    Integer ACCESS_TOKEN_TIME = 3600 * 24;
    Integer REFRESH_TOKEN_TIME = 3600 * 24 * 7;
    String REDIS_DATABASE = "security:auth";
    String REDIS_KEY_AUTHCODE = "authCode";
    //验证码超期时间
    Long REDIS_EXPIRE_CODE = 90L;
    Long REDIS_EXPIRE_COMMON = 86400L;
}
