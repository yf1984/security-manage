package xu.yishan.security.common.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @Description: 用户登录参数
 * @author: YF
 * @date: 2021/5/28
 */
@Data
public class SysAdminParam {
    @ApiModelProperty(value = "用户名", required = true)
    @NotEmpty
    private String username;
    @ApiModelProperty(value = "密码", required = true)
    @NotEmpty
    private String password;
    @ApiModelProperty(value = "验证码", required = true)
//    @NotEmpty
    private String code;

    @ApiModelProperty(value = "uuid", required = true)
//    @NotEmpty
    private String uuid;
}
