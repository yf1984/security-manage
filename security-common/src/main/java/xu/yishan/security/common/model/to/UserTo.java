package xu.yishan.security.common.model.to;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * @Description: 登录用户信息
 * @author: YF
 * @date: 2021/5/25
 */
@Data
@EqualsAndHashCode
@NoArgsConstructor
@ToString
public class UserTo {
    private Long userId;
    private String username;
    private String password;
    private Byte status;
    private String clientId;
    private List<String> roles;
}
