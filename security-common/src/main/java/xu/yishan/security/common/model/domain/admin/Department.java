package xu.yishan.security.common.model.domain.admin;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * @TableName department
 */
@TableName(value = "department")
@Data
public class Department implements Serializable {
    /**
     *
     */
    @TableId
    private Long id;
    /**
     *
     */
    private Long parentId;
    /**
     * 部门名称
     */
    private String departmentName;

    /**
     * 助记码
     */
    private String mnemonicCode;

    /**
     * 联系人
     */
    private String contacts;

    /**
     * 电话
     */
    private String phone;

    /**
     * 地址
     */
    private String address;

    /**
     *
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    /**
     *
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    /**
     * 创建者
     */
    private Long creator;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private boolean hasChildren;

    @TableField(exist = false)
    private List<Department> children;

}