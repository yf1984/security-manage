package xu.yishan.security.user.service.impl;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xu.yishan.security.common.constant.AdminConstant;
import xu.yishan.security.common.constant.AuthConstant;
import xu.yishan.security.common.exception.CustomException;
import xu.yishan.security.common.model.domain.admin.SysMenu;
import xu.yishan.security.common.model.domain.admin.SysRole;
import xu.yishan.security.common.model.domain.admin.SysUser;
import xu.yishan.security.common.model.vo.AuthToken;
import xu.yishan.security.common.model.vo.PasswordForm;
import xu.yishan.security.common.model.vo.SysAdminParam;
import xu.yishan.security.common.result.R;
import xu.yishan.security.common.result.ResultCode;
import xu.yishan.security.common.valid.AddGroup;
import xu.yishan.security.user.feign.AuthFeignClient;
import xu.yishan.security.user.mapper.SysUserMapper;
import xu.yishan.security.user.service.*;

import javax.validation.constraints.NotBlank;
import java.awt.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
    @Autowired
    private AuthFeignClient authFeignClient;
    @Autowired
    private SysUserRoleService userRoleService;
    @Autowired
    private SysRoleService roleService;
    @Autowired
    private UserCacheService cacheService;
    @Autowired
    private SysMenuService menuService;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public String login(SysAdminParam param) {
        String username = param.getUsername();
        String password = param.getPassword();
        String code = param.getCode();
        String uuid = param.getUuid();
        if (StrUtil.isEmpty(username) || StrUtil.isEmpty(password)) {
            throw new CustomException(ResultCode.USERNAME_OR_PASSWORD_NULL);
        }
        // 校验验证码
        if (StrUtil.isEmpty(code) || StrUtil.isEmpty(uuid)) {
            throw new CustomException(ResultCode.CODE_ERROR);
        }
        String redisCode = cacheService.getAuthCode(uuid);
        if (!StrUtil.equalsIgnoreCase(redisCode, code)) {
            throw new CustomException(ResultCode.CODE_ERROR);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("client_id", AuthConstant.ADMIN_CLIENT_ID);
        params.put("client_secret", "123456");
        params.put("grant_type", "password");
        params.put("username", username);
        params.put("password", password);
        R result = authFeignClient.getAccessToken(params);
        if (result.getSuccess() && result.getData() != null) {
            // 将token存入redis
            AuthToken authToken = Convert.convert(AuthToken.class, result.getData());
            cacheService.saveToken(authToken);
            //将jti返回
            return authToken.getJwi();
        }
        return null;
    }

    @Override
    public SysUser loadUserByUsername(String username) {
        //获取用户信息
        SysUser user = getUserByUsername(username);
        if (BeanUtil.isNotEmpty(user)) {
            //根据用户ID获取角色
            List<SysRole> roleList = getRoleList(user.getUserId());
            if (CollUtil.isNotEmpty(roleList)) {
                //根据角色获取权限列表
                List<SysMenu> menuList = menuService.listByRoleList(roleList);
                if (CollUtil.isNotEmpty(menuList)) {
                    Set<String> perms = menuList.stream().map(SysMenu::getPerms).collect(Collectors.toSet());
                    user.setPermList(CollUtil.newArrayList(perms));
                }
            }
        }
        return user;
    }

    @Override
    public List<SysRole> getRoleList(Long userId) {
        List<Long> roleIds = userRoleService.getRoleIdsByUserId(userId);
        if (CollUtil.isNotEmpty(roleIds)) {
            return roleService.listByIds(roleIds);
        }
        return null;
    }

    /*
     * @Author YF
     * @Description 获取当前登录用户
     * @Date 9:30 2021/6/29
     * @return xu.yishan.security.common.model.domain.admin.SysUser
     **/
    @Override
    public SysUser getLoginUser() {
        OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
        String tokenValue = details.getTokenValue();
        if (StrUtil.isNotBlank(tokenValue)) {
            Claims claims = Jwts.parser()
                    .setSigningKey("auth".getBytes(StandardCharsets.UTF_8))
                    .parseClaimsJws(tokenValue)
                    .getBody();
            Long userId = claims.get("userId", Long.class);
            if (userId == null) {
                throw new CustomException(ResultCode.LOGIN_AUTH);
            }
            SysUser user = getById(userId);
            if (user == null) {
                throw new CustomException(ResultCode.FETCH_USERINFO_ERROR);
            }
            return user;
        } else {
            throw new CustomException(ResultCode.LOGIN_AUTH);
        }
    }

    @Override
    public SysUser getUserByUsername(String username) {
        return this.baseMapper.getUserByUsername(username);
    }

    @Override
    public Image getCaptcha(String uuid) {
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(200, 100);
        String code = lineCaptcha.getCode();
        //验证码存入redis
        cacheService.setAuthCode(uuid, code);
        //返回验证码图片
        return lineCaptcha.createImage(code);
    }


    @Override
    public void logout(String jti) {
        cacheService.removeToken(jti);
    }

    @Override
    @Transactional
    public boolean initPassword(Long[] userIds) {
        String password = passwordEncoder.encode(AdminConstant.INIT_PASSWORD);
        List<SysUser> userList = listByIds(CollUtil.toList(userIds));
        userList.forEach(user -> user.setPassword(password));
        return updateBatchById(userList);
    }

    @Override
    public Page<SysUser> getPage(Long page, Long limit, String username) {
        Page<SysUser> userPage = new Page<>(page, limit);
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        if (StrUtil.isNotEmpty(username)) {
            wrapper.like("username", username);
        }
        //只有超级管理员才能查询所有用户
        SysUser sysUser = getLoginUser();
        if (!sysUser.getUserId().equals(AdminConstant.SUPER_ADMIN)) {
            wrapper.eq("department_id", sysUser.getCreateUserId());
        }
        return page(userPage, wrapper);
    }

    @Override
    @Transactional
    public boolean changePassword(PasswordForm form) {
        String password = form.getPassword();
        String newPassword = form.getNewPassword();
        if (StrUtil.isBlank(newPassword)) {
            throw new CustomException(ResultCode.NEW_PASSWORD_NULL);
        }
        Long userId = getLoginUser().getUserId();
        SysUser user = getById(userId);
        if (passwordEncoder.matches(password, user.getPassword())) {
            if (StrUtil.equals(password, newPassword)) {
                throw new CustomException(ResultCode.NEW_OLD_PASSWORD_SAME);
            }
            user.setPassword(passwordEncoder.encode(newPassword));
            return updateById(user);
        } else {
            throw new CustomException(ResultCode.PARAM_ERROR);
        }
    }

    @Override
    @Transactional
    public boolean saveUser(SysUser user) {
        String password = user.getPassword().trim();
        String confirmPassword = user.getConfirmPassword().trim();
        if (!StrUtil.equals(password, confirmPassword)) {
            throw new CustomException(ResultCode.CONFIRM_PASSWORD_DIFFERENT);
        }
        user.setCreateUserId(getLoginUser().getUserId());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        boolean save = save(user);
        //保存用户与角色关系
        if (save) {
            userRoleService.saveOrUpdateRelation(user.getUserId(), user.getRoleIdList());
        }
        return save;
    }

    @Override
    @Transactional
    public boolean updateUser(SysUser user) {
        SysUser sysUser = getById(user.getUserId());
        @NotBlank(groups = AddGroup.class) String password = sysUser.getPassword();
        BeanUtil.copyProperties(user, sysUser);
        //确保原密码不会被修改
        sysUser.setPassword(password);
        //保存用户与角色关系
        userRoleService.saveOrUpdateRelation(user.getUserId(), user.getRoleIdList());
        return updateById(user);
    }

    @Override
    public boolean deleteBatchUser(Long[] userIds) {
        if (ArrayUtil.contains(userIds, AdminConstant.SUPER_ADMIN)) {
            throw new CustomException(ResultCode.ADMIN_NOT_DELETE);
        }
        if (ArrayUtil.contains(userIds, getLoginUser().getUserId())) {
            throw new CustomException(ResultCode.LOGIN_USER_NOT_DELETE);
        }
        //删除和角色关联关系
        userRoleService.deleteBatchByUserId(CollUtil.toList(userIds));
        return removeByIds(CollUtil.toList(userIds));
    }
}
