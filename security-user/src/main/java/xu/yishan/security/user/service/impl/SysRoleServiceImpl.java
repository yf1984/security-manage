package xu.yishan.security.user.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xu.yishan.security.common.constant.AdminConstant;
import xu.yishan.security.common.exception.CustomException;
import xu.yishan.security.common.model.domain.admin.SysRole;
import xu.yishan.security.common.result.ResultCode;
import xu.yishan.security.user.mapper.SysRoleDao;
import xu.yishan.security.user.service.SysRoleMenuRelationService;
import xu.yishan.security.user.service.SysRoleService;
import xu.yishan.security.user.service.SysUserRoleService;
import xu.yishan.security.user.service.SysUserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yf on 2021/4/1
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleDao, SysRole> implements SysRoleService {
    @Autowired
    private SysUserService userService;
    @Autowired
    private SysRoleMenuRelationService roleMenuRelationService;
    @Autowired
    private SysUserRoleService userRoleService;

    @Override
    public Page<SysRole> page(Long pageNum, Long pageSize, String roleName) {
        QueryWrapper<SysRole> wrapper = new QueryWrapper<>();
        Long userId = userService.getLoginUser().getUserId();
        Page<SysRole> page = new Page<>(pageNum, pageSize);
        if (StrUtil.isNotBlank(roleName)) {
            wrapper.like("role_name", roleName);
        }
        if (!AdminConstant.SUPER_ADMIN.equals(userId)) {
            wrapper.eq("create_user_id", userId);
        }
        return this.page(page, wrapper);
    }

    @Override
    public List<SysRole> select() {
        Map<String, Object> map = new HashMap<>();
        //如果不是超级管理员，则只查询自己所拥有的角色列表
        Long userId = userService.getLoginUser().getUserId();
        if (!AdminConstant.SUPER_ADMIN.equals(userId)) {
            map.put("create_user_id", userId);
        }
        return listByMap(map);
    }

    @Override
    public SysRole info(Long roleId) {
        SysRole role = this.getById(roleId);
        //查询角色对应的菜单
        List<Long> menus = roleMenuRelationService.getMenusByRoleIds(CollUtil.toList(roleId));
        role.setMenuIdList(menus);
        return role;
    }

    @Override
    @Transactional
    public boolean saveRole(SysRole role) {
        role.setCreateUserId(userService.getLoginUser().getUserId());
        boolean save = this.save(role);
        if (save && CollUtil.isNotEmpty(role.getMenuIdList())) {
            roleMenuRelationService.saveOrUpdate(role.getRoleId(), role.getMenuIdList());
        }
        return save;
    }

    @Override
    public boolean updateRole(SysRole role) {
        if (role.getRoleId().equals(AdminConstant.SUPER_ROLE)) {
            throw new CustomException(ResultCode.BASE_ROLE_NOT_UPDATE);
        }
        roleMenuRelationService.saveOrUpdate(role.getRoleId(), role.getMenuIdList());
        return updateById(role);
    }

    @Override
    public boolean delete(Long[] roleIds) {
        if (ArrayUtil.contains(roleIds, AdminConstant.SUPER_ROLE)) {
            throw new CustomException(ResultCode.BASE_ROLE_NOT_DELETE);
        }
        //删除角色与菜单关联
        roleMenuRelationService.deleteBatch(CollUtil.toList(roleIds));
        //删除角色与用户关联
        userRoleService.deleteBatchByRoleId(CollUtil.toList(roleIds));
        //删除角色
        return this.removeByIds(CollUtil.toList(roleIds));
    }
}
