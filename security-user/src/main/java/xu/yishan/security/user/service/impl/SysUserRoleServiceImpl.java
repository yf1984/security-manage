package xu.yishan.security.user.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xu.yishan.security.common.constant.AdminConstant;
import xu.yishan.security.common.model.domain.admin.SysUserRole;
import xu.yishan.security.user.mapper.SysUserRoleDao;
import xu.yishan.security.user.service.SysUserRoleService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yf on 2021/4/1
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleDao, SysUserRole> implements SysUserRoleService {
    @Override
    public List<Long> getRoleIdsByUserId(Long userId) {
        return this.baseMapper.getRoleIdsByUserId(userId);
    }

    @Override
    public void saveOrUpdateRelation(Long userId, List<Long> roleIdList) {
        //先删除和用户关联的角色
        deleteBatchByUserId(CollUtil.toList(userId));
        //添加新的关联关系
        roleIdList.add(AdminConstant.SUPER_ROLE);
        List<SysUserRole> userRoles = new ArrayList<>();
        for (Long roleId : roleIdList) {
            SysUserRole userRole = new SysUserRole();
            userRole.setRoleId(roleId);
            userRole.setUserId(userId);
            userRoles.add(userRole);
        }
        saveBatch(userRoles);
    }

    @Override
    public void deleteBatchByRoleId(List<Long> roleIds) {
        QueryWrapper<SysUserRole> wrapper = new QueryWrapper<>();
        wrapper.in("role_id", roleIds);
        remove(wrapper);
    }

    @Override
    @Transactional
    public void deleteBatchByUserId(List<Long> userIds) {
        QueryWrapper<SysUserRole> wrapper = new QueryWrapper<>();
        wrapper.in("user_id", userIds);
        remove(wrapper);
    }


}
