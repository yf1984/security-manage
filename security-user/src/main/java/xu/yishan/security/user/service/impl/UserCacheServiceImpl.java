package xu.yishan.security.user.service.impl;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import xu.yishan.security.common.model.vo.AuthToken;
import xu.yishan.security.user.service.UserCacheService;

import java.util.concurrent.TimeUnit;

import static xu.yishan.security.common.constant.AuthConstant.*;

/**
 * Created by yf on 2021/4/2
 */
@Service
public class UserCacheServiceImpl implements UserCacheService {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public void removeToken(String jti) {
        String key = REDIS_DATABASE + ":" + jti;
        redisTemplate.delete(key);
    }

    @Override
    public AuthToken getToken(String jti) {
        String key = REDIS_DATABASE + ":" + jti;
        String tokenStr = redisTemplate.boundValueOps(key).get();
        return JSON.parseObject(tokenStr, AuthToken.class);
    }

    @Override
    public void saveToken(AuthToken token) {
        String key = REDIS_DATABASE + ":" + token.getJwi();
        redisTemplate.boundValueOps(key).set(JSON.toJSONString(token), ACCESS_TOKEN_TIME, TimeUnit.SECONDS);
    }

    @Override
    public void setAuthCode(String uuid, String authCode) {
        String key = REDIS_DATABASE + ":" + REDIS_KEY_AUTHCODE + ":" + uuid;
        redisTemplate.boundValueOps(key).set(authCode, REDIS_EXPIRE_CODE, TimeUnit.SECONDS);
    }

    @Override
    public String getAuthCode(String uuid) {
        String key = REDIS_DATABASE + ":" + REDIS_KEY_AUTHCODE + ":" + uuid;
        return redisTemplate.boundValueOps(key).get();
    }
}
