package xu.yishan.security.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xu.yishan.security.common.model.domain.admin.SysUser;

import javax.annotation.ManagedBean;

@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
    SysUser getUserByUsername(@Param("username") String username);
}
