package xu.yishan.security.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Mapper;
import xu.yishan.security.common.model.domain.admin.SysRole;

import java.util.List;

/**
 * Created by yf on 2021/4/1
 */
public interface SysRoleService extends IService<SysRole> {
    Page<SysRole> page(Long pageNum, Long pageSize, String roleName);

    List<SysRole> select();

    SysRole info(Long roleId);

    boolean saveRole(SysRole role);

    boolean updateRole(SysRole role);

    boolean delete(Long[] roleIds);
}
