package xu.yishan.security.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;
import xu.yishan.security.common.model.domain.admin.SysRole;
import xu.yishan.security.common.model.domain.admin.SysUser;
import xu.yishan.security.common.model.vo.PasswordForm;
import xu.yishan.security.common.model.vo.SysAdminParam;

import java.awt.*;
import java.util.List;

public interface SysUserService extends IService<SysUser> {
    String login(SysAdminParam adminParam);

    SysUser loadUserByUsername(String username);

    List<SysRole> getRoleList(Long userId);
    SysUser getLoginUser();
    SysUser getUserByUsername(String username);
    Image getCaptcha(String uuid);

    @Transactional
    boolean initPassword(Long[] userIds);

    Page<SysUser> getPage(Long page, Long limit, String username);

    boolean changePassword(PasswordForm form);
    void logout(String jti);

    @Transactional
    boolean saveUser(SysUser user);

    @Transactional
    boolean updateUser(SysUser user);

    boolean deleteBatchUser(Long[] userIds);
}
