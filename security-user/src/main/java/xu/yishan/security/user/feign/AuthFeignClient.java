package xu.yishan.security.user.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xu.yishan.security.common.result.R;

import java.util.Map;

@FeignClient("security-auth")
public interface AuthFeignClient {
    @PostMapping(value = "/oauth/token")
    R getAccessToken(@RequestParam Map<String, String> parameters);
}
