package xu.yishan.security.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xu.yishan.security.common.model.domain.admin.SysRoleMenuRelation;

import java.util.List;

/**
 * @Entity xu.yishan.car.common.model.SysRoleMenuRelation
 */
@Mapper
public interface SysRoleMenuRelationDao extends BaseMapper<SysRoleMenuRelation> {
    List<Long> getMenusByRoleIds(@Param("roleIdList") List<Long> roleIdList);
}