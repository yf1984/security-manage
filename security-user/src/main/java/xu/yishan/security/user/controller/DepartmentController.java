package xu.yishan.security.user.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import xu.yishan.security.common.model.domain.admin.Department;
import xu.yishan.security.common.result.R;
import xu.yishan.security.user.service.DepartmentService;

import java.util.List;

/**
 * 部门controller
 */
@RestController
@RequestMapping("department")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @GetMapping("list")
    @ApiOperation("部门列表")
    @PreAuthorize("hasAuthority('department:list')")
    public R list() {
        List<Department> departmentList = departmentService.getAllList();
        return R.ok(departmentList);
    }
    @PostMapping("save")
    @ApiOperation("保存部门")
    @PreAuthorize("hasAuthority('department:save')")
    public R save(@RequestBody Department department) {
        boolean b=departmentService.saveDepartment(department);
        return R.ok();
    }
    @PostMapping("update")
    @ApiOperation("修改部门")
    @PreAuthorize("hasAuthority('department:update')")
    public R update(@RequestBody Department department) {
        boolean b=departmentService.updateDepartment(department);
        return R.ok();
    }
}




