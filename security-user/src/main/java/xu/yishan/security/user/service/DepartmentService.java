package xu.yishan.security.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import xu.yishan.security.common.model.domain.admin.Department;

import java.util.List;

/**
 *
 */
public interface DepartmentService extends IService<Department> {

    List<Department> getAllList();

    boolean saveDepartment(Department department);

    boolean updateDepartment(Department department);
}
