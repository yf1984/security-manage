package xu.yishan.security.user.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import xu.yishan.security.common.constant.AdminConstant;
import xu.yishan.security.common.exception.CustomException;
import xu.yishan.security.common.model.domain.admin.Department;
import xu.yishan.security.common.model.domain.admin.SysUser;
import xu.yishan.security.common.result.ResultCode;
import xu.yishan.security.user.service.DepartmentService;
import xu.yishan.security.user.mapper.DepartmentMapper;
import org.springframework.stereotype.Service;
import xu.yishan.security.user.service.SysUserService;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department>
        implements DepartmentService {
    @Autowired
    private SysUserService userService;

    @Override
    public List<Department> getAllList() {
        //判断当前登录用户
        SysUser loginUser = userService.getLoginUser();
        List<Department> departmentList;
        if (loginUser.getUserId().equals(AdminConstant.SUPER_ADMIN)) {
            departmentList = listByParentId(0L);
        } else {
            departmentList = listByIds(CollUtil.toList(loginUser.getDepartmentId()));
        }
        getChildren(departmentList);
        return departmentList;
    }

    @Override
    @Transactional
    public boolean saveDepartment(Department department) {
        // 判断新加部门是否是根部门，只能有一个根部门
        if (department.getParentId() == 0) {
            List<Department> parentList = listByParentId(0L);
            if (CollUtil.isNotEmpty(parentList)) {
                throw new CustomException(ResultCode.ROOT_DEPARTMENT_IS_EXIST);
            }
            return save(department);
        }
        return save(department);
    }

    @Override
    @Transactional
    public boolean updateDepartment(Department department) {
        // 判断新加部门是否是根部门，只能有一个根部门
        if (department.getParentId() == 0) {
            List<Department> parentList = listByParentId(0L);
            if (CollUtil.isNotEmpty(parentList)) {
                throw new CustomException(ResultCode.ROOT_DEPARTMENT_IS_EXIST);
            }
            return updateById(department);
        }
        return updateById(department);
    }

    private List<Department> listByParentId(Long parentId) {
        QueryWrapper<Department> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_id", parentId);
        return list(wrapper);
    }

    //递归查找部门
    private void getChildren(List<Department> departmentList) {
        for (Department department : departmentList) {
            List<Department> childList = listByParentId(department.getId());
            if (CollUtil.isNotEmpty(childList)) {
                department.setChildren(childList);
                department.setHasChildren(true);
                getChildren(childList);
            }
        }
    }
}




