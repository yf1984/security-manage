package xu.yishan.security.user.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xu.yishan.security.common.constant.AdminConstant;
import xu.yishan.security.common.model.domain.admin.SysMenu;
import xu.yishan.security.common.model.domain.admin.SysRole;
import xu.yishan.security.common.model.domain.admin.SysRoleMenuRelation;
import xu.yishan.security.common.model.domain.admin.SysUser;
import xu.yishan.security.user.mapper.SysMenuDao;
import xu.yishan.security.user.service.SysMenuService;
import xu.yishan.security.user.service.SysRoleMenuRelationService;
import xu.yishan.security.user.service.SysUserRoleService;
import xu.yishan.security.user.service.SysUserService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by yf on 2021/4/2
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuDao, SysMenu> implements SysMenuService {
    @Autowired
    private SysUserRoleService userRoleService;
    @Autowired
    private SysRoleMenuRelationService roleMenuRelationService;
    @Autowired
    private SysUserService userService;
    @Autowired
    private ResourceRolesService resourceRolesService;

    @Override
    public List<SysMenu> getUserMenuList(Long id) {
        if (AdminConstant.SUPER_ADMIN.equals(id)) {
            return getAllMenuList(null);
        }
        List<Long> roleIdList = userRoleService.getRoleIdsByUserId(id);
        List<Long> menuIdList = roleMenuRelationService.getMenusByRoleIds(roleIdList);
        return getAllMenuList(menuIdList);
    }

    /**
     * 获取所有菜单列表
     */
    private List<SysMenu> getAllMenuList(List<Long> menuIdList) {
        //查询根菜单列表
        List<SysMenu> menuList = queryListParentId(0L, menuIdList);
        //递归查询获取子菜单
        getMenuTreeList(menuList, menuIdList);
        return menuList;
    }

    /**
     * 递归
     */
    private List<SysMenu> getMenuTreeList(List<SysMenu> menuList, List<Long> menuIdList) {
        List<SysMenu> subMenuList = new ArrayList<>();
        for (SysMenu menu : menuList) {
            //目录
            if (menu.getType() == AdminConstant.MenuType.CATALOG.getValue()) {
                menu.setList(getMenuTreeList(queryListParentId(menu.getMenuId(), menuIdList), menuIdList));
            }
            subMenuList.add(menu);
        }
        return subMenuList;
    }

    @Override
    public List<SysMenu> queryListParentId(Long parentId, List<Long> menuIdList) {
        List<SysMenu> menuList = getMenuListByParentId(parentId);
        if (CollUtil.isEmpty(menuIdList)) {
            return menuList;
        }
        List<SysMenu> userMenuList = new ArrayList<>();
        for (SysMenu sysMenu : menuList) {
            if (menuIdList.contains(sysMenu.getMenuId())) {
                userMenuList.add(sysMenu);
            }
        }
        return userMenuList;
    }

    @Override
    public List<SysMenu> getMenuListByParentId(Long parentId) {
        return this.baseMapper.getMenuListByParentId(parentId);
    }

    /**
     * 获取用户权限列表
     *
     * @param userId 用户ID
     * @return 访问权限
     */
    @Override
    public Set<String> getUserPermissions(Long userId) {
        List<SysMenu> menuList;
        if (userId.equals(AdminConstant.SUPER_ADMIN)) {
            menuList = this.list();
        } else {
            List<Long> roleIdList = userRoleService.getRoleIdsByUserId(userId);
            List<Long> menuIds = roleMenuRelationService.getMenusByRoleIds(roleIdList);
            menuList = listByIds(menuIds);
        }
        Set<String> permissions = new HashSet<>();
        menuList.forEach(menu -> {
            String perms = menu.getPerms();
            String[] split = StrUtil.split(perms, ",");
            permissions.addAll(Arrays.asList(split));
        });
        return permissions;
    }


    @Override
    public List<SysMenu> getNotButtonList() {
        return baseMapper.getNotButtonList();
    }

    @Override
    public boolean deleteMenu(Long menuId) {
        //删除角色和菜单关联
        roleMenuRelationService.deleteByMenuId(menuId);
        boolean b = removeById(menuId);
        if (b) {
            resourceRolesService.initResourceRolesMap();
        }
        return b;
    }

    @Override
    public List<SysMenu> getList() {
        //如果是管理员，获取所有菜单
        SysUser sysUser = userService.getLoginUser();
        if (sysUser.getUserId().equals(AdminConstant.SUPER_ADMIN)) {
            return list();
        } else {
            //获取当然用户菜单
            List<Long> roleIdList = userRoleService.getRoleIdsByUserId(sysUser.getUserId());
            List<Long> menuIdList = roleMenuRelationService.getMenusByRoleIds(roleIdList);
            QueryWrapper<SysMenu> wrapper = new QueryWrapper<>();
            if (CollUtil.isNotEmpty(menuIdList)) {
                wrapper.in("menu_id", menuIdList);
                return list(wrapper);
            }
            return null;
        }
    }

    @Override
    @Transactional
    public boolean saveMenu(SysMenu menu) {
        //保存菜单和管理员关联
        boolean save = save(menu);
        if (save) {
            SysRoleMenuRelation relation = new SysRoleMenuRelation();
            relation.setMenuId(menu.getMenuId());
            relation.setRoleId(AdminConstant.SUPER_ROLE);
            roleMenuRelationService.save(relation);
        }
        if (StrUtil.isNotBlank(menu.getPerms())) {
            resourceRolesService.initResourceRolesMap();
        }
        return save;
    }

    @Override
    public boolean updateMenu(SysMenu menu) {
        SysMenu sysMenu = getById(menu.getMenuId());
        BeanUtil.copyProperties(menu, sysMenu);
        boolean b = updateById(sysMenu);
        if (b) {
            resourceRolesService.initResourceRolesMap();
        }
        return b;
    }

    @Override
    public List<SysMenu> getMenuList(int value) {
        QueryWrapper<SysMenu> wrapper = new QueryWrapper<>();
        wrapper.ne("type", value);
        return list(wrapper);
    }

    @Override
    public List<SysMenu> listByRoleList(List<SysRole> roleList) {
        List<Long> menuIdList = roleMenuRelationService.getMenusByRoleIds(
                roleList.stream().map(SysRole::getRoleId).collect(Collectors.toList())
        );
        if (CollUtil.isNotEmpty(menuIdList)) {
            return listByIds(menuIdList);
        }
        return null;
    }
}
