package xu.yishan.security.user.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xu.yishan.security.common.model.domain.admin.SysRoleMenuRelation;
import xu.yishan.security.user.mapper.SysRoleMenuRelationDao;
import xu.yishan.security.user.service.SysMenuService;
import xu.yishan.security.user.service.SysRoleMenuRelationService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yf on 2021/4/2
 */
@Service
public class SysRoleMenuRelationServiceImpl extends ServiceImpl<SysRoleMenuRelationDao, SysRoleMenuRelation> implements SysRoleMenuRelationService {
    @Autowired
    private SysMenuService menuService;
    @Autowired
    private ResourceRolesService resourceRolesService;

    @Override
    public List<Long> getMenusByRoleIds(List<Long> roleIdList) {
        return this.baseMapper.getMenusByRoleIds(roleIdList);
    }

    @Override
    public void deleteByMenuId(Long menuId) {
        QueryWrapper<SysRoleMenuRelation> wrapper = new QueryWrapper<>();
        wrapper.eq("menu_id", menuId);
        remove(wrapper);
    }

    @Override
    public boolean saveOrUpdate(Long roleId, List<Long> menuIdList) {
        //先删除角色与菜单关系
        this.deleteBatch(CollUtil.toList(roleId));
        if (CollUtil.isNotEmpty(menuIdList)) {
            //保存角色与菜单关系
            ArrayList<SysRoleMenuRelation> list = new ArrayList<>();
            for (Long menuId : menuIdList) {
                SysRoleMenuRelation relation = new SysRoleMenuRelation();
                relation.setMenuId(menuId);
                relation.setRoleId(roleId);
                list.add(relation);
            }
            boolean b = this.saveBatch(list);
            if (b) {
                //更新redis中缓存权限规则
                resourceRolesService.initResourceRolesMap();
            }
            return b;
        }
        return false;
    }

    @Override
    public boolean deleteBatch(List<Long> roleIds) {
        QueryWrapper<SysRoleMenuRelation> wrapper = new QueryWrapper<>();
        wrapper.in("role_id", roleIds);
        return remove(wrapper);
    }
}
