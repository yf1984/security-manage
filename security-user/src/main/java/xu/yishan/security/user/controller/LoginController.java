package xu.yishan.security.user.controller;

import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import xu.yishan.security.common.model.vo.PasswordForm;
import xu.yishan.security.common.model.vo.SysAdminParam;
import xu.yishan.security.common.result.R;
import xu.yishan.security.common.result.ResultCode;
import xu.yishan.security.user.service.SysUserService;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;

/**
 * Created by yf on 2021/4/1
 */
@RestController
@Api(tags = "登录管理")
@RequestMapping("login")
public class LoginController {
    @Autowired
    private SysUserService userService;

    @ApiOperation(value = "登录以后返回token")
    @PostMapping(value = "/login")
    public R login(@RequestBody SysAdminParam adminParam) {
        String jti = userService.login(adminParam);
        if (jti == null) {
            return R.failed(ResultCode.LOGIN_FAILED);
        }
        return R.ok(jti);
    }


    @ApiOperation("验证码")
    @GetMapping("/captcha.jpg")
    public void captcha(HttpServletResponse response, String uuid) throws IOException {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");
        Image image = userService.getCaptcha(uuid);
        ServletOutputStream out = response.getOutputStream();
        ImgUtil.writeJpg(image, out);
        IOUtils.closeQuietly(out);
    }


    @ApiOperation(value = "登出功能")
    @PostMapping(value = "/logout")
    public R logout(@RequestBody String jti) {
        userService.logout(jti);
        return R.ok();
    }
}
