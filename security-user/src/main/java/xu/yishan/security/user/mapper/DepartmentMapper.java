package xu.yishan.security.user.mapper;

import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xu.yishan.security.common.model.domain.admin.Department;

/**
 * @Entity xu.yishan.security.user.Department
 */
@Mapper
public interface DepartmentMapper extends BaseMapper<Department> {

}




