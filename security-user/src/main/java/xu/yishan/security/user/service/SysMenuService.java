package xu.yishan.security.user.service;


import com.baomidou.mybatisplus.extension.service.IService;
import xu.yishan.security.common.model.domain.admin.SysMenu;
import xu.yishan.security.common.model.domain.admin.SysRole;

import java.util.List;
import java.util.Set;

/**
 * Created by yf on 2021/4/2
 */
public interface SysMenuService extends IService<SysMenu> {
    List<SysMenu> getUserMenuList(Long id);


    List<SysMenu> queryListParentId(Long parentId, List<Long> menuIdList);

    List<SysMenu> getMenuListByParentId(Long parentId);

    Set<String> getUserPermissions(Long userId);


    List<SysMenu> getNotButtonList();

    boolean deleteMenu(Long menuId);

    List<SysMenu> getList();

    boolean saveMenu(SysMenu menu);

    boolean updateMenu(SysMenu menu);

    List<SysMenu> getMenuList(int value);

    List<SysMenu> listByRoleList(List<SysRole> roleList);
}
