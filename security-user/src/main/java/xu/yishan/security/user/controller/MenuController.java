package xu.yishan.security.user.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import xu.yishan.security.common.model.domain.admin.SysMenu;
import xu.yishan.security.common.model.domain.admin.SysUser;
import xu.yishan.security.common.result.R;
import xu.yishan.security.user.service.SysMenuService;
import xu.yishan.security.user.service.SysUserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by yf on 2021/4/2
 */
@RestController
@RequestMapping("/menu")
public class MenuController {
    @Autowired
    private SysMenuService menuService;
    @Autowired
    private SysUserService userService;


    @ApiOperation("导航菜单")
    @GetMapping("/nav")
    public R nav() {
        //获取当前登录用户信息
        SysUser user = userService.getLoginUser();
        //获取登录用户对应的菜单和权限
        List<SysMenu> menuList = menuService.getUserMenuList(user.getUserId());
        Set<String> permissionList = menuService.getUserPermissions(user.getUserId());
        Map<String, Object> map = new HashMap<>();
        map.put("menuList", menuList);
        map.put("permissions", permissionList);
        return R.ok(map);
    }

    @ApiOperation("保存菜单")
    @PostMapping("/save")
    @PreAuthorize("hasAuthority('menu:save')")
    public R saveMenu(@RequestBody SysMenu menu) {
        boolean b = menuService.saveMenu(menu);
        return b ? R.ok() : R.failed();
    }

    @ApiOperation("所有菜单列表")
    @GetMapping("/list")
    @PreAuthorize("hasAuthority('menu:list')")
    public R list() {
        List<SysMenu> menuList = menuService.getList();
        for (SysMenu sysMenu : menuList) {
            SysMenu menu = menuService.getById(sysMenu.getParentId());
            if (BeanUtil.isNotEmpty(menu)) {
                sysMenu.setParentName(menu.getName());
            }
        }
        return R.ok(menuList);
    }

    @ApiOperation("选择菜单(添加、修改菜单)")
    @GetMapping("select")
    @PreAuthorize("hasAuthority('menu:select')")
    public R select() {
        //查询列表数据
        List<SysMenu> menuList = menuService.getNotButtonList();
        SysMenu root = new SysMenu();
        root.setMenuId(0L);
        root.setName("一级菜单");
        root.setParentId(-1L);
        root.setOpen(true);
        menuList.add(root);
        return R.ok(menuList);
    }

    @ApiOperation("菜单信息")
    @GetMapping("info/{menuId}")
    @PreAuthorize("hasAuthority('menu:info')")
    public R info(@PathVariable("menuId") Long menuId) {
        SysMenu menu = menuService.getById(menuId);
        if (BeanUtil.isNotEmpty(menu)) {
            return R.ok(menu);
        }
        return R.failed();
    }

    @ApiOperation("修改菜单")
    @PostMapping("/update")
    @PreAuthorize("hasAuthority('menu:update')")
    public R update(@RequestBody SysMenu menu) {
        boolean update = menuService.updateMenu(menu);
        return update ? R.ok() : R.failed();
    }

    @ApiOperation("删除菜单")
    @PostMapping("delete/{menuId}")
    @PreAuthorize("hasAuthority('menu:delete')")
    public R delete(@PathVariable("menuId") Long menuId) {
        if (menuId <= 20) {
            return R.failed("系统菜单，不能删除");
        }
        //判断是否有子菜单或按钮
        List<SysMenu> menuList = menuService.getMenuListByParentId(menuId);
        if (CollUtil.isNotEmpty(menuList)) {
            return R.failed("请先删除子菜单或按钮");
        }
        boolean b = menuService.deleteMenu(menuId);
        return b ? R.ok() : R.failed();
    }

//    @ApiOperation("初始化权限规则")
//    @GetMapping("/initResourceRolesMap")
//    public Map<String, List<String>> initResourceRolesMap() {
//        return resourceRolesService.initResourceRolesMap();
//    }
}
