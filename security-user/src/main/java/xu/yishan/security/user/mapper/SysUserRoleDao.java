package xu.yishan.security.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xu.yishan.security.common.model.domain.admin.SysUserRole;

import java.util.List;

/**
 * 用户与角色对应关系
 * 
 * @author yf
 * @email 279221684@qq.com
 * @date 2021-03-29 10:20:56
 */
@Mapper
public interface SysUserRoleDao extends BaseMapper<SysUserRole> {

    List<Long> getRoleIdsByUserId(@Param("userId") Long userId);
}
