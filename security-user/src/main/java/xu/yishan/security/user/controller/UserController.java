package xu.yishan.security.user.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import xu.yishan.security.common.model.domain.admin.SysUser;
import xu.yishan.security.common.model.vo.PasswordForm;
import xu.yishan.security.common.result.R;
import xu.yishan.security.common.valid.AddGroup;
import xu.yishan.security.common.valid.UpdateGroup;
import xu.yishan.security.user.service.SysUserService;

@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    private SysUserService userService;

    @ApiOperation("根据用户名获取通用用户信息")
    @GetMapping(value = "/loadByUsername")
    public R loadUserByUsername(@RequestParam String username) {
        SysUser sysUser = userService.loadUserByUsername(username);
        return R.ok(sysUser);
    }

    @ApiOperation("修改登录用户密码")
    @PreAuthorize("hasAuthority('user:update')")
    @PostMapping("password")
    public R changePassword(@RequestBody PasswordForm form) {
        boolean b = userService.changePassword(form);
        return b ? R.ok() : R.failed();
    }
//    @ApiOperation("根据用户ID获取用户信息")
//    @PostMapping(value = "/infoById")
//    public R infoById(@RequestParam("userId") Long userId) {
//        SysUser user = userService.getById(userId);
//        //获取用户所属的角色列表
//        List<Long> roleIdList = userRoleService.getRoleIdsByUserId(userId);
//        user.setRoleIdList(roleIdList);
//        return R.ok(user);
//    }


    @ApiOperation("获取登录的用户信息")
    @GetMapping("/info")
    @PreAuthorize("hasAuthority('user:info')")
    public R info() {
        SysUser user = userService.getLoginUser();
        return R.ok(user);
    }

    @ApiOperation("用户列表")
    @GetMapping("page")
    @PreAuthorize("hasAuthority('user:page')")
    public R<Page<SysUser>> page(@RequestParam("page") Long page, @RequestParam("limit") Long limit,
                                 @RequestParam(value = "username", required = false) String username) {
        Page<SysUser> userPage = userService.getPage(page, limit, username);
        return R.ok(userPage);
    }

    @ApiOperation("保存用户")
    @PostMapping("save")
    @PreAuthorize("hasAuthority('user:save')")
    public R saveUser(@RequestBody @Validated(AddGroup.class) SysUser user) {
        boolean b = userService.saveUser(user);
        return b ? R.ok() : R.failed();
    }

    @ApiOperation("修改用户")
    @PostMapping("update")
    @PreAuthorize("hasAuthority('user:update')")
    public R updateUser(@RequestBody @Validated(UpdateGroup.class) SysUser user) {
        boolean b = userService.updateUser(user);
        return b ? R.ok() : R.failed();
    }

    @ApiOperation("删除用户")
    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('user:delete')")
    public R deleteUser(@RequestBody Long[] userIds) {
        boolean b = userService.deleteBatchUser(userIds);
        return b ? R.ok() : R.failed();
    }

    @ApiOperation("初始化密码")
    @PostMapping("/initPassword")
    @PreAuthorize("hasAuthority('user:initPassword')")
    public R initPassword(@RequestBody Long[] userIds) {
        boolean b = userService.initPassword(userIds);
        return b ? R.ok() : R.failed();
    }
}
