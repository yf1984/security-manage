package xu.yishan.security.user.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import xu.yishan.security.common.model.domain.admin.SysRole;
import xu.yishan.security.common.result.R;
import xu.yishan.security.user.service.SysRoleService;

import java.util.List;

/**
 * Created by yf on 2021/4/6
 */
@RestController
@RequestMapping("role")
public class RoleController {
    @Autowired
    private SysRoleService roleService;


    @GetMapping("/page")
    @ApiOperation("角色分页列表")
    @PreAuthorize("hasAuthority('role:page')")
    public R page(@RequestParam(value = "page", defaultValue = "1") Long pageNum,
                  @RequestParam(value = "limit", defaultValue = "5") Long pageSize,
                  @RequestParam(value = "roleName", required = false) String roleName) {
        Page<SysRole> page = roleService.page(pageNum, pageSize, roleName);
        return R.ok(page);
    }

    @ApiOperation("角色列表")
    @GetMapping("select")
    @PreAuthorize("hasAuthority('role:select')")
    public R select() {
        List<SysRole> list = roleService.select();
        return R.ok(list);
    }

    @ApiOperation("角色信息")
    @GetMapping("info/{roleId}")
    @PreAuthorize("hasAuthority('role:info')")
    public R info(@PathVariable("roleId") Long roleId) {
        SysRole role = roleService.info(roleId);
        return R.ok(role);
    }

    @ApiOperation("保存角色")
    @PostMapping("save")
    @PreAuthorize("hasAuthority('role:save')")
    public R saveRole(@RequestBody SysRole role) {
        boolean b = roleService.saveRole(role);
        return b ? R.ok() : R.failed();
    }
    @ApiOperation("修改角色")
    @PostMapping("update")
    @PreAuthorize("hasAuthority('role:update')")
    public R updateRole(@RequestBody SysRole role){
        boolean b = roleService.updateRole(role);
        return b ? R.ok() : R.failed();
    }
    @ApiOperation("删除角色")
    @PostMapping("delete")
    @PreAuthorize("hasAuthority('role:delete')")
    public  R delete(@RequestBody Long[] roleIds){
        boolean b = roleService.delete(roleIds);
        return b ? R.ok() : R.failed();
    }
}
