package xu.yishan.security.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import xu.yishan.security.common.model.domain.admin.SysRoleMenuRelation;

import java.util.List;

/**
 * Created by yf on 2021/4/2
 */
public interface SysRoleMenuRelationService extends IService<SysRoleMenuRelation> {
    List<Long> getMenusByRoleIds(List<Long> roleIdList);

    void deleteByMenuId(Long menuId);

    boolean saveOrUpdate(Long roleId, List<Long> menuIdList);

    boolean deleteBatch(List<Long> roleIds);
}
