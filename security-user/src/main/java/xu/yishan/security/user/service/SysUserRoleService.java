package xu.yishan.security.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;
import xu.yishan.security.common.model.domain.admin.SysUserRole;

import java.util.List;

/**
 * Created by yf on 2021/4/1
 */
public interface SysUserRoleService extends IService<SysUserRole> {
    List<Long> getRoleIdsByUserId(Long userId);

    void saveOrUpdateRelation(Long userId, List<Long> roleIdList);

    @Transactional
    void deleteBatchByRoleId(List<Long> roleIds);

    @Transactional
    void deleteBatchByUserId(List<Long> userIds);
}
