package xu.yishan.security.user.service;


import xu.yishan.security.common.model.vo.AuthToken;

/**
 * 后台用户缓存操作类
 * Created by yf on 2021/4/2
 */
public interface UserCacheService {
    /**
     * 删除后台用户缓存
     */
    void removeToken(String jti);

    /**
     * 获取缓存后台用户信息
     * @return
     */
    AuthToken getToken(String jti);

    /**
     * 设置缓存后台用户信息
     */
    void saveToken(AuthToken  token);

    void setAuthCode(String uuid, String authCode);

    String getAuthCode(String uuid);
}
