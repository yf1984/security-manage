package xu.yishan.security.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xu.yishan.security.common.model.domain.admin.SysMenu;

import java.util.List;

/**
 * @Entity xu.yishan.car.common.model.SysMenu
 */
@Mapper
public interface SysMenuDao extends BaseMapper<SysMenu> {
    List<SysMenu> getMenuListByParentId(@Param("parentId") Long parentId);

    List<SysMenu> getNotButtonList();
}