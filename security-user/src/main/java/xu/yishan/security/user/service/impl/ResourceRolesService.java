package xu.yishan.security.user.service.impl;

import cn.hutool.core.collection.CollUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;
import xu.yishan.security.common.constant.AdminConstant;
import xu.yishan.security.common.constant.AuthConstant;
import xu.yishan.security.common.model.domain.admin.SysMenu;
import xu.yishan.security.common.model.domain.admin.SysRole;
import xu.yishan.security.common.model.domain.admin.SysRoleMenuRelation;
import xu.yishan.security.user.service.SysMenuService;
import xu.yishan.security.user.service.SysRoleMenuRelationService;
import xu.yishan.security.user.service.SysRoleService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by yf on 2021/4/16
 */
@Service
public class ResourceRolesService {
    @Autowired
    private SysMenuService menuService;
    @Autowired
    private SysRoleService roleService;
    @Autowired
    private SysRoleMenuRelationService roleMenuRelationService;

    @CachePut(value = AuthConstant.RESOURCE_ROLES_MAP_VALUE, key = "#root.methodName")
    public Map<String, List<String>> initResourceRolesMap() {
        Map<String, List<String>> resourceRoleMap = new TreeMap<>();
        //获取除目录外所有菜单列表
        List<SysMenu> menuList = menuService.getMenuList(AdminConstant.MenuType.CATALOG.getValue());
        //获取所有角色列表
        List<SysRole> roleList = roleService.list();
        List<SysRoleMenuRelation> relationList = roleMenuRelationService.list();
        for (SysMenu menu : menuList) {
            Set<Long> roleIds = relationList.stream().filter(item -> item.getMenuId().equals(menu.getMenuId()))
                    .map(SysRoleMenuRelation::getRoleId).collect(Collectors.toSet());
            List<String> roleNames = roleList.stream().filter(item -> roleIds.contains(item.getRoleId()))
                    .map(item -> item.getRoleId() + "_" + item.getRoleName()).collect(Collectors.toList());
            String[] split = menu.getPerms().split(",");
            ArrayList<String> list = CollUtil.toList(split);
            if (CollUtil.isNotEmpty(roleNames)) {
                list.forEach(item -> {
                    //取出map中相同key的值
                    List<String> perm1 = resourceRoleMap.get(item);
                    if (CollUtil.isNotEmpty(perm1)) {
                        //合并两个集合
                        CollUtil.addAllIfNotContains(roleNames, perm1);
                    }
                    //合并后集合放入map
                    resourceRoleMap.put(item, roleNames);
                });
            }
        }
        return resourceRoleMap;
    }
}
