package xu.yishan.security.gateway.filter;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.expression.ParseException;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import xu.yishan.security.common.constant.AuthConstant;
import xu.yishan.security.common.model.vo.AuthToken;
import xu.yishan.security.gateway.config.IgnoreUrlsConfig;
import xu.yishan.security.gateway.service.CacheService;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * 将登录用户的JWT转化成用户信息的全局过滤器
 * Created by yf on 2021/4/1
 */
@Component
public class AuthGlobalFilter implements GlobalFilter, Ordered {
    @Autowired
    private IgnoreUrlsConfig config;
    @Autowired
    private CacheService cacheService;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //查看是否白名单路径，白名单路径放行
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        URI uri = request.getURI();
        AntPathMatcher pathMatcher = new AntPathMatcher();
        //白名单路径移除JWT请求头
        List<String> urls = config.getUrls();
        for (String url : urls) {
            if (pathMatcher.match(url, uri.getPath())) {
//                request = exchange.getRequest().mutate().header(AuthConstant.JWT_TOKEN_HEADER, "").build();
//                exchange = exchange.mutate().request(request).build();
                return chain.filter(exchange);
            }
        }
        //非白名单路径，从redis取出jwt令牌，放入header放行
        String jti = exchange.getRequest().getHeaders().getFirst(AuthConstant.TOKEN_HEADER);
        if (StrUtil.isEmpty(jti)) {
            // jti为空拒绝访问
            return out(response);
        }
        try {
            //从redis取出token
            AuthToken token = cacheService.getToken(jti);
            if (token == null) {
                return out(response);
            }
            request = request.mutate().header(AuthConstant.JWT_TOKEN_HEADER, AuthConstant.JWT_TOKEN_PREFIX + token.getAccess_token()).build();
            exchange = exchange.mutate().request(request).build();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }

    private Mono<Void> out(ServerHttpResponse response) {

        JSONObject message = new JSONObject();
        message.put("success", false);
        message.put("code", 28004);
        message.put("data", "鉴权失败");
        byte[] bits = message.toString().getBytes(StandardCharsets.UTF_8);
        DataBuffer buffer = response.bufferFactory().wrap(bits);
        //response.setStatusCode(HttpStatus.UNAUTHORIZED);
        //指定编码，否则在浏览器中会中文乱码
        response.getHeaders().add("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        return response.writeWith(Mono.just(buffer));
    }
}
