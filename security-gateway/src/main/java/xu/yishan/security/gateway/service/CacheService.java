package xu.yishan.security.gateway.service;


import xu.yishan.security.common.model.vo.AuthToken;

/**
 * 后台用户缓存操作类
 * Created by yf on 2021/4/2
 */
public interface CacheService {

    /**
     * 获取缓存后台用户信息
     */
    AuthToken getToken(String jti);


}
