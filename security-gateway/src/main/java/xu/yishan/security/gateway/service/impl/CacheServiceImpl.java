package xu.yishan.security.gateway.service.impl;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import xu.yishan.security.common.model.vo.AuthToken;
import xu.yishan.security.gateway.service.CacheService;

import java.util.concurrent.TimeUnit;

import static xu.yishan.security.common.constant.AuthConstant.*;

/**
 * Created by yf on 2021/4/2
 */
@Service
public class CacheServiceImpl implements CacheService {
    @Autowired
    private StringRedisTemplate redisTemplate;


    @Override
    public AuthToken getToken(String jti) {
        String key = REDIS_DATABASE + ":" + jti;
        String tokenStr = redisTemplate.boundValueOps(key).get();
        return JSON.parseObject(tokenStr, AuthToken.class);
    }
}
